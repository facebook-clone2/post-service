package com.example.postservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
@CrossOrigin
@Slf4j
public class MessageController {
    @MessageMapping("/broadcast")
    @SendTo("/topic/reply")
    public void broadcastMessage(@Payload String message) {
        log.info("Inside broadcastMessage function to send message: {}", message);
//        return message;
    }

    @MessageMapping("/user-message")
    @SendToUser("/queue/reply")
    public String sendBackToUser(@Payload String message, @Header("simpSessionId") String sessionId) {
        log.info("Got session Id: {}", sessionId);
        return "Only you have received this message: " + message;
    }

}
