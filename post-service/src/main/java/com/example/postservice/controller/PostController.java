package com.example.postservice.controller;

import com.example.postservice.model.PostModel;
import com.example.postservice.model.PostResponseModel;
import com.example.postservice.service.IPostService;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/v1.0/post")
public class PostController {
    @Autowired private IPostService postService;
    @PostMapping(value = "/create-post")
    public ResponseEntity<String> createPost(@RequestBody PostModel postModel){
        log.info("Got call to create new post: postModel is: {}", postModel);
        return  postService.createPost(postModel);
    }
    @GetMapping(value = "/get-post")
    public ResponseEntity<List<PostResponseModel>> getAllPostOfUser(@RequestParam Integer userId){
        log.info("Got call to get all the post of user, with userId: {}", userId);
       return postService.getAllPostOfUser(userId);
    }
    @GetMapping(value = "/get-story")
    public ResponseEntity<List<PostResponseModel>> getAllStoryOfUser(@RequestParam Integer userId){
        log.info("Got call to get all the story of user, with userId: {}", userId);
        return postService.getAllStoryOfUser(userId);
    }

    @DeleteMapping(value = "/delete-post")
    public ResponseEntity deletePostByPostId(@RequestParam String postId){
        return postService.deletePostById(postId);
    }

    @PostMapping(value = "/getAllPostByUserIds")
    public List<PostModel> getAllPostByUserIds(@RequestBody List<Integer> userIds) {
        log.info("Got call to get post of userIds: {}", userIds);
        return postService.getAllPostByUserIds(userIds);
    }
    @PostMapping(value = "/getAllStoryByUserIds")
    public List<PostModel> getAllStoryByUserIds(@RequestBody List<Integer> userIds) {
        log.info("Got call to get stories of userIds: {}", userIds);
        return postService.getAllStoryByUserIds(userIds);
    }
}
