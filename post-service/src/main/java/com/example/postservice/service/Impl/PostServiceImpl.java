package com.example.postservice.service.Impl;

import com.example.postservice.entity.PostEntity;
import com.example.postservice.model.PostModel;
import com.example.postservice.model.PostResponseModel;
import com.example.postservice.repository.PostRepository;
import com.example.postservice.service.IPostService;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class PostServiceImpl implements IPostService {
    @Autowired
    private PostRepository postRepository;

    @Autowired private RestTemplate restTemplate;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    public Date getCurrentDateTime() {
        LocalDateTime localDateTime = LocalDateTime.now();
        return java.sql.Timestamp.valueOf(localDateTime);
    }

    public ResponseEntity<String> createPost(PostModel post) {
        log.info("Got call to create new post: {}", post);
        ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);

        PostEntity postEntity = new PostEntity();
        postEntity.setUserId(post.getUserId());
        postEntity.setText(post.getText());
        postEntity.setContentType(post.getContentType());
        postEntity.setContentUrl(post.getContentUrl());
        postEntity.setIsStory(post.getIsStory());
        Date currentDate = getCurrentDateTime();
        postEntity.setCreatedAt(currentDate);
        postEntity.setUpdatedAt(currentDate);
        PostEntity res = postRepository.save(postEntity);
        String name = restTemplate.postForObject("http://10.65.1.188:8081/v1.0/users/getUserName",post.getUserId(),String.class);
        log.info("Name for userId: {} is: {}", post.getUserId(), name);
        if(res != null){
            log.info("Got call to send message to clients");
            messagingTemplate.convertAndSend("/topic/reply", "New post created by user: " + name);
            log.info("After, send message to clients");
        }
        log.info("Created post entity to save in Database: {}", res);
        response = new ResponseEntity<>(res.getId(), HttpStatus.CREATED);
        return response;
    }

    public ResponseEntity<List<PostResponseModel>> getAllPostOfUser(Integer userId) {
        log.info("Got Call to get all the post of UserId: {}", userId);
        ResponseEntity<List<PostResponseModel>> response = new ResponseEntity<>(HttpStatus.OK);
        List<PostEntity> res = postRepository.findByUserIdAndIsStory(userId, Boolean.FALSE);
        log.info("Got result from res: {}", res);
        List<PostResponseModel> postResponseList = new ArrayList<>();
        for (PostEntity postEntity : res){
            PostResponseModel postResponseModel = new PostResponseModel();
            postResponseModel.setId(postEntity.getId());
            postResponseModel.setUserId(postEntity.getUserId());
            postResponseModel.setText(postEntity.getText());
            postResponseModel.setContentType(postEntity.getContentType());
            postResponseModel.setContentUrl(postEntity.getContentUrl());
            postResponseModel.setIsStory(postEntity.getIsStory());
            postResponseModel.setCreatedAt(postEntity.getCreatedAt());
            postResponseModel.setUpdatedAt(postEntity.getUpdatedAt());
            postResponseList.add(postResponseModel);
        }
        response = new ResponseEntity<>(postResponseList, HttpStatus.OK);
        return response;
    }

    public ResponseEntity<List<PostResponseModel>> getAllStoryOfUser(Integer userId) {
        log.info("Got Call to get all the post of UserId: {}", userId);
        ResponseEntity<List<PostResponseModel>> response = new ResponseEntity<>(HttpStatus.OK);
//        List<PostEntity> res = postRepository.findByUserIdAndIsStory(userId, Boolean.TRUE);
        Date currentDate = getCurrentDateTime();
        log.info("Current Data is: {}", currentDate);
        Date startTime = new Date(currentDate.getTime() - (10 * 60 * 1000)); // 10 minutes ago
        log.info("Start Time is : {}", startTime);
        List<PostEntity> res = postRepository.findByUserIdAndIsStoryAndCreatedAtGreaterThanEqual(userId, Boolean.TRUE, startTime);

        log.info("Got result from res: {}", res);
        List<PostResponseModel> postResponseList = new ArrayList<>();
        for (PostEntity postEntity : res){
            PostResponseModel postResponseModel = new PostResponseModel();
            postResponseModel.setId(postEntity.getId());
            postResponseModel.setUserId(postEntity.getUserId());
            postResponseModel.setText(postEntity.getText());
            postResponseModel.setContentType(postEntity.getContentType());
            postResponseModel.setContentUrl(postEntity.getContentUrl());
            postResponseModel.setIsStory(postEntity.getIsStory());
            postResponseModel.setCreatedAt(postEntity.getCreatedAt());
            postResponseModel.setUpdatedAt(postEntity.getUpdatedAt());

            postResponseList.add(postResponseModel);
        }
        response = new ResponseEntity<>(postResponseList, HttpStatus.OK);
        return response;
    }

    public ResponseEntity deletePostById(@RequestParam String postId){
        log.info("Got call to delete post with postId: {}", postId);
        postRepository.deleteById(postId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public List<PostModel> getAllPostByUserIds(List<Integer> userIds){
        List<PostModel> postModels=new ArrayList<>();
        for(Integer userId:userIds){
            String name = restTemplate.postForObject("http://10.65.1.188:8081/v1.0/users/getUserName",userId,String.class);
            if(name == null){
                name = "DeletedUser";
            }
            log.info("Name for userId: {} is: {}", userId, name);
            List<PostEntity> postEntities = postRepository.findByUserIdAndIsStory(userId, Boolean.FALSE);
            for(PostEntity postEntity:postEntities){
                PostModel postModel = new PostModel();
                postModel.setId(postEntity.getId());
                postModel.setName(name);
                postModel.setUserId(postEntity.getUserId());
                postModel.setText(postEntity.getText());
                postModel.setContentType(postEntity.getContentType());
                postModel.setContentUrl(postEntity.getContentUrl());
                postModel.setIsStory(postEntity.getIsStory());
                postModels.add(postModel);
            }
        }
        log.info("Got postmodels from database: {}", postModels);
        return postModels;
    }
    public List<PostModel> getAllStoryByUserIds(List<Integer> userIds){
        log.info("Got call to get stories by userIds: {}", userIds);
        List<PostModel> postModels=new ArrayList<>();
        for(Integer userId:userIds){
            String name = restTemplate.postForObject("http://10.65.1.188:8081/v1.0/users/getUserName",userId,String.class);
            if(name == null){
                name = "DeletedUser";
            }
            log.info("Name for userId: {} is: {}", userId, name);
            Date currentDate = getCurrentDateTime();
            log.info("Current Data is: {}", currentDate);
//            Date currentTime = new Date();
            Date startTime = new Date(currentDate.getTime() - (10 * 60 * 1000)); // 10 minutes ago
            log.info("Start Time is : {}", startTime);
//            List<PostEntity> postEntities = postRepository.findByUserIdAndIsStory(userId, Boolean.TRUE);
            List<PostEntity> postEntities = postRepository.findByUserIdAndIsStoryAndCreatedAtGreaterThanEqual(userId, Boolean.TRUE, startTime);
            for(PostEntity postEntity:postEntities){
                PostModel postModel = new PostModel();
                postModel.setId(postEntity.getId());
                postModel.setName(name);
                postModel.setUserId(postEntity.getUserId());
                postModel.setText(postEntity.getText());
                postModel.setContentType(postEntity.getContentType());
                postModel.setContentUrl(postEntity.getContentUrl());
                postModel.setIsStory(postEntity.getIsStory());
                postModels.add(postModel);
            }
        }
        log.info("Got stories from database: {}", postModels);
        return postModels;
    }

}
