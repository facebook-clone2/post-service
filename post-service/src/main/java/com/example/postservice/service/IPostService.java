package com.example.postservice.service;

import com.example.postservice.model.PostModel;
import com.example.postservice.model.PostResponseModel;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IPostService {
    ResponseEntity<String> createPost(PostModel post);
    ResponseEntity<List<PostResponseModel>> getAllPostOfUser(Integer userId);
    ResponseEntity<List<PostResponseModel>> getAllStoryOfUser(Integer userId);

    ResponseEntity deletePostById(String postId);

    List<PostModel> getAllPostByUserIds(List<Integer> userIds);

    List<PostModel> getAllStoryByUserIds(List<Integer> userIds);
}
