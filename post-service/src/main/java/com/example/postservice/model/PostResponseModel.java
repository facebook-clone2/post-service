package com.example.postservice.model;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class PostResponseModel {
    private String id;

    private Integer userId;

    private String text;

    private String contentType;

    private String contentUrl;

    private Date createdAt;

    private Date updatedAt;

    private Boolean isStory;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIsStory() {
        return isStory;
    }

    @Override
    public String toString() {
        return "PostResponseModel{" +
                "id='" + id + '\'' +
                ", userId=" + userId +
                ", text='" + text + '\'' +
                ", contentType='" + contentType + '\'' +
                ", contentUrl='" + contentUrl + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", isStory=" + isStory +
                '}';
    }

    public void setIsStory(Boolean story) {
        isStory = story;
    }
}
