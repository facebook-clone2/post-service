package com.example.postservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class PostModel {
    private String id;
    private Integer userId;

    private String text;
    private String name;
    private String contentType;

    private String contentUrl;

    public Boolean getIsStory() {
        return isStory;
    }

    public void setIsStory(Boolean story) {
        isStory = story;
    }

    private Boolean isStory;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }




    @Override
    public String toString() {
        return "PostModel{" +
                " userId=" + userId +
                ", text='" + text + '\'' +
                ", contentType='" + contentType + '\'' +
                ", contentUrl='" + contentUrl + '\'' +
                ", isStory=" + isStory +
                '}';
    }
}