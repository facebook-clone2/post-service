package com.example.postservice.repository;

import com.example.postservice.entity.PostEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PostRepository extends MongoRepository<PostEntity, String> {
    List<PostEntity> findByUserIdAndIsStory(Integer userId, Boolean isStory);

    List<PostEntity> findByUserIdAndIsStory(int userId, boolean story);

    List<PostEntity> findByUserIdAndIsStoryAndCreatedAtGreaterThanEqual(Integer userId, Boolean isStory, Date endTime);

}
