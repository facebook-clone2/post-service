package com.example.postservice.entity;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


import java.util.Date;

@Slf4j
@Document(collection = "posts")
public class PostEntity {
    @Id
    private String id;
    @Field(name = "userId")
    private Integer userId;
    @Field(name = "text")
    private String text;
    @Field(name = "contentType")
    private String contentType;
    @Field(name = "contentUrl")
    private String contentUrl;

    @Field(name = "createdAt")
    private Date createdAt;
    @Field(name = "updatedAt")
    private Date updatedAt;
    @Field(name = "isStory")
    private Boolean isStory;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getIsStory() {
        return isStory;
    }

    @Override
    public String toString() {
        return "PostEntity{" +
                "id='" + id + '\'' +
                ", userId=" + userId +
                ", text='" + text + '\'' +
                ", contentType='" + contentType + '\'' +
                ", contentUrl='" + contentUrl + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", isStory=" + isStory +
                '}';
    }

    public void setIsStory(Boolean story) {
        isStory = story;
    }

}
